package tp5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * @author Steven
 */

public class Interface extends JFrame {

	private static final int CONNECTES = 0;
	private static final int CONNEXION = 1;
	private static final int DECONNEXION = 2;
	private static final long	serialVersionUID	= 1L;
	private Style				defaut, other, info;
	private JPanel panelConnecte;
	private JTextPane			conversation;
	private List<String>		messages;
	private List<String>		connectes;
	private String pseudo;

	public Interface(final String pseudo) {
		super("Chat :: " + pseudo);
		this.pseudo = pseudo;
		// Le premier message envoye est le pseudo (traitement cote serveur)
		messages = new ArrayList<String>();
		messages.add(pseudo);
		connectes = new ArrayList<String>();
		connectes.add(pseudo);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(new Dimension(800, 500));
		setResizable(true);
		setLocationRelativeTo(null);

		conversation = new JTextPane();
		conversation.setFont((new JTextField()).getFont());
		conversation.setEditable(false);

		// Code trop jouuuuuuuuuuuli
		defaut = conversation.getStyle("default");
		final Style you = conversation.addStyle("you", defaut);
		other = conversation.addStyle("other", defaut);
		info = conversation.addStyle("info", defaut);

		StyleConstants.setForeground(you, Color.RED);
		StyleConstants.setForeground(other, Color.BLUE);
		StyleConstants.setForeground(info, Color.BLACK);
		you.addAttribute(StyleConstants.CharacterConstants.Bold, Boolean.TRUE);
		other.addAttribute(StyleConstants.CharacterConstants.Bold, Boolean.TRUE);
		info.addAttribute(StyleConstants.CharacterConstants.Bold, Boolean.TRUE);

		final StyledDocument sDoc = (StyledDocument) conversation.getDocument();
		try {
			sDoc.insertString(sDoc.getLength(), getDate() + " *** Connecté sous le nom de " + pseudo + " ***\n", info);
		}
		catch (BadLocationException e) {
		}

		JScrollPane scroller = new JScrollPane(conversation, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		JPanel panelActions = new JPanel(new BorderLayout());

		panelConnecte = new JPanel(new WrapLayout(0, 0, 0));
		panelConnecte.setPreferredSize(new Dimension(100, 100));
		JLabel label = new JLabel(pseudo);
		label.setForeground(Color.RED);
		label.setPreferredSize(new Dimension(80, 25));
		panelConnecte.add(label);
		JScrollPane scrollConnectes = new JScrollPane(panelConnecte, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollConnectes.setBorder(BorderFactory.createTitledBorder("Connectés :"));

		final JTextArea text = new JTextArea();
		text.setFont((new JTextField()).getFont());
		text.setLineWrap(true);

		JButton button = new JButton("Envoyer");
		button.setPreferredSize(new Dimension(75, 75));
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (text.getText().equals(""))
					return;

				try {
					sDoc.insertString(sDoc.getLength(), getDate() + " ", info);
					sDoc.insertString(sDoc.getLength(), pseudo, you);
					sDoc.insertString(sDoc.getLength(), " : " + text.getText() + "\n", defaut);
					messages.add(pseudo + " : " + text.getText());
					text.setText("");
				}
				catch (BadLocationException e1) {
				}
			}

		});

		JScrollPane scrollText = new JScrollPane(text, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		panelActions.add(scrollText, BorderLayout.CENTER);
		panelActions.add(button, BorderLayout.EAST);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(scroller, BorderLayout.CENTER);
		getContentPane().add(scrollConnectes, BorderLayout.EAST);
		getContentPane().add(panelActions, BorderLayout.SOUTH);
		setVisible(true);

	}

	public void update(String message) {
		String[] datas = message.split(" : ");
		StyledDocument sDoc = (StyledDocument) conversation.getDocument();
		try {
			int infoSys = Integer.parseInt(datas[0]);

			switch (infoSys){
			case CONNECTES:
				for(int i = 1; i < datas.length; ++i)
					ajouterConnecte(datas[i]);
				break;
			case CONNEXION:
				try {
					// Le serveur renvoie Pseudo : MSG
					sDoc.insertString(sDoc.getLength(), getDate() + " " + datas[1] + " vient de se connecter.\n", info);
					ajouterConnecte(datas[1]);
				} catch (BadLocationException e1) {}
				break;
			case DECONNEXION:
				try {
					// Le serveur renvoie Pseudo : MSG
					sDoc.insertString(sDoc.getLength(), getDate() + " Déconnexion de " + datas[1] + "\n", info);
					supprimerConnecte(datas[1]);
				} catch (BadLocationException e1) {}
				break;
			default :
				break;
			}
		} catch(Exception e){
			try {
				// Le serveur renvoie Pseudo : MSG
				sDoc.insertString(sDoc.getLength(), getDate() + " ", info);
				sDoc.insertString(sDoc.getLength(), datas[0], other);
				sDoc.insertString(sDoc.getLength(),
						" : " + message.substring(datas[0].length() + " : ".length(), message.length()) + "\n", defaut);
			} catch (BadLocationException e1) {}
		}
	}

	public List<String> getMessages() {
		return messages;
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (InstantiationException e) {
			e.printStackTrace();
		}
		catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		String pseudo = JOptionPane.showInputDialog(null, "Indiquez votre pseudonyme", "Pseudonyme",
				JOptionPane.OK_CANCEL_OPTION);
		String ip = JOptionPane.showInputDialog(null, "Indiquez l'adresse IP", "Adresse IP",
				JOptionPane.OK_CANCEL_OPTION);

		if (pseudo == null || ip == null)
			System.exit(1);

		Interface interf = new Interface(pseudo);

		new client3(pseudo, ip, interf);
	}

	public String getDate(){
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		return "[" + dateFormat.format(calendar.getTime()) + "]";
	}

	public void ajouterConnecte(String connecte){
		connectes.add(connecte);
		JLabel label = new JLabel(connecte);
		label.setForeground(Color.BLUE);
		label.setPreferredSize(new Dimension(80, 25));
		panelConnecte.add(label);
		panelConnecte.revalidate();
		repaint();
	}

	public void supprimerConnecte(String connecte){
		connectes.remove(connecte);
		panelConnecte.removeAll();
		for(String pseudo : connectes){
			JLabel label = new JLabel(pseudo);
			if (pseudo.equals(this.pseudo))
				label.setForeground(Color.RED);
			else
				label.setForeground(Color.BLUE);
			label.setPreferredSize(new Dimension(80, 20));
			panelConnecte.add(label);
		}
		panelConnecte.revalidate();
		repaint();
	}
}
