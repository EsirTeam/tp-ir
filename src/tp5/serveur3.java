package tp5;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class serveur3 {

	private static final int CONNECTES = 0;
	private static final int CONNEXION = 1;
	private static final int DECONNEXION = 2;

	public static void main(String argv[]) throws Exception {
		System.out.println("*** Lancement du serveur ***");
		Selector selector = Selector.open();
		ServerSocketChannel server = ServerSocketChannel.open();
		server.configureBlocking(false);
		server.socket().bind(new InetSocketAddress(2000));

		server.register(selector, SelectionKey.OP_ACCEPT);
		List<Client> clients = new ArrayList<Client>();
		Charset charset = Charset.forName("UTF-8");
		while (true) {
			selector.select();
			//int noOfKeys = selector.select();

			Set<SelectionKey> selectedKeys = selector.selectedKeys();
			Iterator<SelectionKey> iter = selectedKeys.iterator();

			while (iter.hasNext()) {

				SelectionKey ky = iter.next();

				if (ky.isAcceptable()) {

					// Accept the new client connection
					SocketChannel socketClient = server.accept();
					socketClient.configureBlocking(false);
					SelectionKey socketKey = socketClient.register(selector, SelectionKey.OP_READ
							| SelectionKey.OP_WRITE);
					Client cl = new Client(new Random().nextInt(), socketClient);
					clients.add(cl);
					socketKey.attach(cl);
					System.out.println("Accepted new connection from client: " + socketClient);
					StringBuilder st = new StringBuilder(CONNECTES + " : ");
					for (Client client : clients) {
						if (client.hasNick())
							st.append(client.getPseudo() + " : ");
					}
					st.delete(st.length() - " : ".length(), st.length());

					socketClient.write(charset.encode(st.toString()));

				}
				else if (ky.isReadable()) {
					// Read the data from client
					SocketChannel socketClient = (SocketChannel) ky.channel();
					ByteBuffer buffer = ByteBuffer.allocate(256);
					String output;
					try {
						socketClient.read(buffer);
						output = new String(buffer.array()).trim();
					}
					catch (Exception e) {
						output = "Bye.";
					}
					String nickClient = "";

					boolean departureClient = false;
					if (output.length() > 0) {
						if (output.equals("Bye.")) {
							socketClient.close();
							clients.remove((Client) ky.attachment());
							departureClient = true;
							nickClient = ((Client) ky.attachment()).getPseudo();
						}
						boolean newClient = false;

						for (Client client : clients) {
							if (client.getSocket().equals(socketClient) && !client.hasNick()) {
								client.setPseudo(output);
								client.setHasNick(true);
								newClient = true;
								nickClient = client.getPseudo();
							}
						}
						if (newClient) {
							String connected = CONNEXION + " : " + nickClient;
							System.out.println(connected);
							for (Client client : clients) {
								if (!client.getSocket().equals(socketClient)) {
									client.getMessages().add(connected);
								}
							}
						}
						else if (departureClient){
							String connected = DECONNEXION + " : " + nickClient;
							System.out.println(connected);
							for (Client client : clients) {
								if (!client.getSocket().equals(socketClient)) {
									client.getMessages().add(connected);
								}
							}
						}
						else {
							for (Client client : clients) {
								if (!client.getSocket().equals(socketClient) && ((Client) ky.attachment()).hasNick()) {
									client.getMessages().add(output);
								}
							}
						}
					}
				}
				else if (ky.isWritable()) {
					SocketChannel socketClient = (SocketChannel) ky.channel();
					for (Client client : clients) {
						if (client.getSocket().equals(socketClient)) {
							for (String s : client.getMessages()) {
								socketClient.write(charset.encode(s));
							}
							client.getMessages().clear();
						}
					}

				}
				else if (!ky.isValid()) {
					SocketChannel client = (SocketChannel) ky.channel();
					client.close();
					clients.remove((Client) ky.attachment());
					System.out.println("Client crashed");
				}
				iter.remove();
			}
		}
	}
}

class Client {
	private Integer			id;
	private String			pseudo;
	private List<String>	messages;
	private SocketChannel	socket;
	private boolean			hasNick;

	public Client(Integer id, SocketChannel socket) {
		this.setId(id);
		this.setSocket(socket);
		messages = new ArrayList<String>();
		this.setHasNick(false);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<String> getMessages() {
		return messages;
	}

	public SocketChannel getSocket() {
		return socket;
	}

	public void setSocket(SocketChannel socket) {
		this.socket = socket;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public boolean hasNick() {
		return hasNick;
	}

	public void setHasNick(boolean hasNick) {
		this.hasNick = hasNick;
	}

}