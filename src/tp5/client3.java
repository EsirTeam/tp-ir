package tp5;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

public class client3 {

	private final static int	PORT	= 2000;

	public client3(String pseudo, String ip, Interface interf) {

		SocketChannel socket = null;
		try {
			Selector selector = Selector.open();

			socket = SocketChannel.open();
			socket.configureBlocking(false);
			socket.connect(new InetSocketAddress(ip, PORT));

			socket.register(selector, SelectionKey.OP_CONNECT);

			// ReadableByteChannel in = Channels.newChannel(System.in);
			Charset charset = Charset.forName("UTF-8");

			while (socket.isOpen()) {
				Set<SelectionKey> selectedKeys = selector.selectedKeys();
				int nbSel = selector.select();
				if (nbSel > 0) {
					Iterator<SelectionKey> keyIterator = selectedKeys.iterator();

					while (keyIterator.hasNext()) {
						SelectionKey selectionKey = keyIterator.next();
						if (selectionKey.isConnectable()) {
							SocketChannel channel = (SocketChannel) selectionKey.channel();
							channel.configureBlocking(false);
							if (channel.finishConnect()) {
								System.out.println("Connecté !");
								socket.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
								continue;
							}
						}
						else if (selectionKey.isReadable()) {
							SocketChannel socketClient = (SocketChannel) selectionKey.channel();
							ByteBuffer buffer = ByteBuffer.allocate(256);
							String output;
							socketClient.read(buffer);
							output = new String(buffer.array()).trim();
							interf.update(output);
						}
						else if (selectionKey.isWritable()) {
							SocketChannel sChannel = (SocketChannel) selectionKey.channel();
							for (String s : interf.getMessages()) {
								sChannel.write(charset.encode(s));
								if (s.equalsIgnoreCase("bye.")) {
									socket.close();
								}
							}
							interf.getMessages().clear();
						}
						keyIterator.remove();
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String argv[]) {
		String host = "127.0.0.1";
		String nom = "Guillaume";

		new client3(nom, host, null);
	}
}
