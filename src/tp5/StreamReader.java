package tp5;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;

/** classe encapsulant un flux InputStream, afin de pouvoir l'utiliser avec un
 *  sélecteur
 *  
 *
 *  Les lectures sont effectués dans un thread à part, les données sont
 *  redirigées sur un Pipe.
 *
 *  La fonction .getChannel() permet d'obtenir le canal de sortie du pipe
 *  qui pourra être utilisée avec un sélecteur.
 */
public class StreamReader
{
	Pipe		pipe;
	ReaderThread	thread;

	public StreamReader (InputStream stream)
		throws IOException
	{
		// création du pipe
		pipe	= Pipe.open();

		// lancement du thread
		thread	= new ReaderThread (stream, pipe.sink());
		thread.start();
	}

	public Pipe.SourceChannel getChannel()
		throws IOException
	{
		// canal de sortie du pipe
		return pipe.source();
	}

	private static class ReaderThread extends Thread
	{
		InputStream stream;
		WritableByteChannel pipe;

		ReaderThread (InputStream stream, WritableByteChannel pipe)
		{
			this.setDaemon(true);
			this.stream	= stream;
			this.pipe	= pipe;
		}

		public void run()
		{
			// création du buffer
			byte[] bytes = new byte [1024];
			ByteBuffer buffer = ByteBuffer.wrap(bytes);

			try {
				while(true)
				{
					// réception des données sur le flux entrée
					int nb = stream.read(bytes);
					if (nb <= 0) //EOF
						break;

					// envoie des données sur le pipe
					buffer.rewind();
					buffer.limit(nb);
					pipe.write(buffer);
				}
				stream.close();
				pipe.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
}
