package tp1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;

public class serveur {
	Thread	thread;

	public static void main(String args[]) {
		UDPServer udpServer = new UDPServer();
		new Thread(udpServer).start();
	}

}

class UDPServer implements Runnable {

	DatagramChannel	dc;

	@Override
	public void run() {
		DatagramChannel dc = null;
		try {
			dc = DatagramChannel.open();
			InetSocketAddress isa = new InetSocketAddress(8888);
			dc.socket().bind(isa);
		}
		catch (IOException e) {
			e.printStackTrace();
			return;
		}
		ByteBuffer rec = ByteBuffer.allocate(2048);
		ByteBuffer send = null;
		Charset charset = Charset.forName("UTF-8");
		while (true) {
			SocketAddress sender;
			try {
				sender = dc.receive(rec);
				rec.flip();
				System.out.println("RECEIVED : " + charset.decode(rec).toString());
				rec.flip();
				send = rec;
				dc.send(send, sender);
				send.clear();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}