package tp1;


import java.nio.channels.*;
import java.nio.charset.*;
import java.net.*;
import java.util.Scanner;
import java.nio.ByteBuffer;
import java.nio.ByteBuffer.*;


public class client {



	public static void main(String[] argv) throws Exception
	{
		// Cr�ez un objet Charset pour g�rer l'encodage et le d�codage du texte
		Charset ch = Charset.forName("UTF-8");


		// Cr�ez votre socket UDP (DatagramChannel)
		DatagramChannel socketUDP  = DatagramChannel.open();
		socketUDP.bind(new InetSocketAddress(8888));

		InetSocketAddress addr =new InetSocketAddress("192.168.43.219",8888);

		while(true)
		{
			// Lisez un ligne au clavier
			Scanner sc = new Scanner(System.in);
			String dataToSend ="";
			System.out.println("Enter sentence:");

			sc = new Scanner(sc.nextLine());

			while (sc.hasNext())
				dataToSend+=sc.next();
			if(dataToSend.equals("exit")){
				break;
			}
			// Envoyez la cha�ne � votre correspondant
			
			socketUDP.send(ch.encode(dataToSend) , addr);
			
			//Check reception 
			ByteBuffer dataReceived =  ByteBuffer.allocate(2048);
			
			socketUDP.receive(dataReceived);
			dataReceived.flip();
			System.out.println("accus� de reception :" + ch.decode(dataReceived));
		}
		socketUDP.close();
		System.out.println("Au revoir ");
	}


}
