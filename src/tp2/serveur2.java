package tp2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class serveur2 {

	public static void main(String argv[]) throws Exception {
		// Service qui va gérer les threads
		ExecutorService threadPool = Executors.newCachedThreadPool();
		// Socket server écoutant sur le port 8888
		ServerSocket serverSocket = new ServerSocket(8888);

		while (true) {
			// On récupère la connexion entrante
			Socket connectionSocket = serverSocket.accept();
			// On demande au service d'éxécuter une nouvelle instance de
			// ServerRunnable qui prendra la socket en paramètre
			threadPool.execute(new ServerRunnable(connectionSocket));
		}
	}

}

/**
 * Class implémentant Runnable et pouvant dont être éxécutée par un Thread.
 *
 */
class ServerRunnable implements Runnable {
	Socket	connectionSocket;

	public ServerRunnable(Socket connectionSocket) {
		this.connectionSocket = connectionSocket;
	}

	/**
	 * Méthode appelée lors de l'éxécution du Thread
	 */
	@Override
	public void run() {
		try {
			// On récupère la requête reçue
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			String OS = null, IP = null;
			String s;
			String file = "";
			// On lit les données présentes dans le buffer
			while ((s = inFromClient.readLine()) != null) {
				System.out.println(s);
				// On va par la suite parser les différents champs utiles par la
				// suite
				if (s.toLowerCase().contains("get")) {
					file = s.replace("GET ", "").replace("HTTP/1.1", "").replace("HTTP/1.0", "").trim();
				}
				if (s.toLowerCase().contains("get")) {
					file = s.replace("GET ", "").replace("HTTP/1.1", "").replace("HTTP/1.0", "").trim();
				}
				if (s.toLowerCase().contains("user-agent")) {
					OS = s.substring(s.indexOf("(") + 1, s.indexOf(")"));
				}
				if (s.toLowerCase().contains("host")) {
					IP = s.substring(s.indexOf(":") + 1, s.indexOf(":", s.indexOf(":") + 1));
				}
				if (s.isEmpty()) {
					break;
				}
			}
			// On créer le buffer qui nous serivra à écrire la réponse au client
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));

			// Si un chemin est spécifié
			if (!file.equals("/") && !file.equals("")) {
				// On récupère le chemin courant
				String currentDir = System.getProperty("user.dir");
				file = currentDir + file;
				File f = new File(file);
				// On vérifie que l'on à le droit de lire le fichier
				if (!checkFileRead(f)) {
					// Si on a pas le droit, on affiche l'erreur 403
					getHTTPHeader(out, "403 Forbidden", f.lastModified());
					out.write("<TITLE>403 forbidden</TITLE>");
					out.write("<P>403 forbidden</P>");
				}
				// On vérifie que le fichier existe et que ce ne soit pas un
				// répertoire
				else if (f.exists() && !f.isDirectory()) {
					// On lit le fichier
					String fileContent = new String(Files.readAllBytes(Paths.get(file)));
					getHTTPHeader(out, "200 Ok", f.lastModified());
					out.write("<TITLE>" + f.getName() + "</TITLE>");
					// On écrit le contenu du fichier au client
					out.write(fileContent);
				}
				// On est dans le cas d'un fichier non existant
				else {
					getHTTPHeader(out, "404 Not Found", 0);
					out.write("<TITLE>404 not found</TITLE>");
					out.write("<P>404 not found</P>");
				}
			}
			// Aucun chemin spécifié, on affiche un message par défaut
			else {
				getHTTPHeader(out, "200 Ok", 0);
				out.write("<TITLE>Exemple</TITLE>");
				out.write("<P>Bonjour,</P>");
				out.write("Votre adresse IP : " + IP + "<br>");
				out.write("Votre OS : " + OS);
			}
			// On ferme les différentes ressources ouvertes
			out.close();
			inFromClient.close();
			connectionSocket.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Méthode de vérification des droits de lecture d'un fichier
	 * 
	 * @param file
	 * @return true si le fichier peut être lu, false sinon
	 */
	public boolean checkFileRead(File file) {
		if (file.exists()) {
			if (!file.canRead())
				return false;
			try {
				// Si le fichier éxiste et que l'on peut le lire, on essaye
				FileReader fileReader = new FileReader(file.getAbsolutePath());
				fileReader.read();
				fileReader.close();
			}
			catch (Exception e) {
				// Si la lecture précédente a echouée, on ne peut pas lire le
				// fichier et on renvoie false
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @param out
	 *            Le Buffer qui sert à écrire au client
	 * @param code
	 *            le code du header HTTP à retourner
	 * @param time
	 *            la date de dernière modification du fichier
	 * @throws IOException
	 */
	void getHTTPHeader(BufferedWriter out, String code, long time) throws IOException {
		// On utilise HTTP/1.0
		out.write("HTTP/1.0 " + code + "\r\n");
		// On récupère la date actuelle
		out.write("Date: " + getServerTime(0, time) + "\r\n");
		out.write("Server: JAVA/Socket\r\n");
		out.write("Content-Type: text/html\r\n");
		// Expiration de la page
		out.write("Expires: " + getServerTime(1, time) + "\r\n");
		// Date de dernière modification de la page
		out.write("Last-modified: " + getServerTime(2, time) + "\r\n");
		out.write("\r\n");
	}

	/**
	 * 
	 * @param mode
	 *            utilisé pour différencier les différents cas
	 * @param time
	 *            utilisé pour forcer la date à un autre date que l'actuelle
	 * @return
	 */
	String getServerTime(int mode, long time) {
		// On récupère une instance de calendar
		Calendar calendar = Calendar.getInstance();
		switch (mode) {
		case 0:// Cas par défaut, la date actuelle
			break;
		case 1:// Cas de l'expiration
				// On dit que la page expirera dans une minute
			calendar.add(Calendar.MINUTE, 1);
			break;
		case 2:// Cas de la date de dernière modification d'un fichier
				// Si la date est précisée
			if (time != 0)
				calendar.setTimeInMillis(time);// On change la date du Calendar
												// par celle-ci
			break;
		}
		// On formate le Calendar correctement
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.FRANCE);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+2"));
		// On renvoie le String formaté
		return dateFormat.format(calendar.getTime());
	}
}
