package tp2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class client2 {

	public static void main(String argv[]) {
		try {
			int port = 8888;
			// Creation d'un client TCP sur l'adresse local 127.0.0.1 et sur le
			// numéro de port indiqué au dessus
			Socket clientSocket = new Socket("127.0.0.1", port);

			Scanner scanner = new Scanner(System.in);
			String fichier = "";
			String str = "";
			// On demande si le client veut lire un fichier html (ou autre)
			System.out.println("Voulez vous lire un fichier HTML ? O/N");
			do {
				str = "";
				// On récupère la saisie au clavier
				str = scanner.nextLine();
				if (str.equals("O")) {
					// Si oui, On demande le chemin du fichier
					System.out.println("Veuillez entrer le chemin du fichier html");
					// On récupère le chemin saisi
					fichier = scanner.nextLine();
				}
				else if (!str.equals("N")) {
					// Si la réponse n'est pas O ou N alors on boucle jusqu'à
					// avoir une réponse correcte
					System.out.println("Veuillez entrer O ou N !");
				}
			}
			while (!str.equals("") && !str.equals("O") && !str.equals("N"));
			scanner.close();

			// On ouvre un PrintWriter pour envoyer la requête au serveur
			PrintWriter toServer = new PrintWriter(clientSocket.getOutputStream(), true);
			// On ouvre un BufferedReader pour lire la réponse du serveur
			BufferedReader fromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			// On ecrit toutes les informations concernant la requête destinée
			// au serveur comme le fait un navigateur web
			toServer.println("GET /" + fichier + " HTTP/1.0"); // Requête HTTP
																// non
																// persistant,
																// avec fichier
																// si cela est
																// voulu
			toServer.println("Host:" + InetAddress.getLoopbackAddress().getHostAddress() + ":" + port); // Envoi
																										// de
																										// l'adresse
																										// IP
																										// et
																										// du
																										// port
			toServer.println("Connection: keep-alive");
			toServer.println("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			toServer.println("Upgrade-Insecure-Requests: 1");
			// Header récupéré de Chrome
			toServer.println("User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.99 Safari/537.36");
			toServer.println("Accept-Encoding: gzip, deflate, sdch");
			toServer.println("Accept-Language: fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4");
			// On ferme la sortie à destination du serveur
			clientSocket.shutdownOutput();
			String reponse;
			// On lit ce que nous répond le serveur
			while ((reponse = fromServer.readLine()) != null) {
				System.out.println(reponse);
			}

			toServer.close();
			fromServer.close();

			// On ferme la connexion TCP avec le serveur
			clientSocket.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
